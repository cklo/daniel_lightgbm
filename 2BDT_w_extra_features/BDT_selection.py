import mplhep as hep

# Imports
import sys
sys.path.append("..")
import pandas as pd
import numpy as np
from Utils_Store import Utils
import toml
from sklearn import preprocessing, model_selection, metrics
import seaborn as sns
import random


import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
#plt.style.use("seaborn-v0_8-whitegrid")
from scipy.special import logit, expit, erf
import os

import optuna
random.seed(42)
from iminuit import Minuit
from scipy import stats
mpl.rcParams.update({
    'text.usetex': False,
    'font.family': 'stixgeneral',
    'mathtext.fontset': 'stix',
    })

import tables
import h5py

import scipy.integrate as integrate

#--------------------------------------------------------
channel = "muon"
#--------------------------------------------------------

# Prefix and abbreviations
# =========
if channel == "muon":
    tree_name    = "Nominal/BaseSelection_KStarMuMu_BmumuKstSelection"
    prefix       = "BmumuKst"
    lepton       = "muon"
    lepton_big   = "Muon"
    lepton_short = "mu"
    lepton_short_double = "mumu"
    antilepton   = "antimuon"
elif channel == "electron":
    tree_name    = "Nominal/BaseSelection_KStaree_BeeKstSelection"
    prefix       = "BeeKst"
    lepton       = "electron"
    lepton_big   = "Electron"
    lepton_short = "e"
    lepton_short_double = "ee"
    antilepton   = "positron"
else:
    print ('"channel" should be either "electron" or "muon"')
    quit()

# Pre-selection cuts
# ============
if channel == "electron":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 5000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    GSF_pT_low         = 5000
    GSF_eta_limit      = 2.5
    sideband_L = 4000
    sideband_R = 5700
    mc_NonresBd        = 300590
    mc_NonresBd_str    = "300590"
    mc_NonresBdbar     = 300591
    mc_NonresBdbar_str = "300591"
    mc_ResBd           = 300592
    mc_ResBd_str       = "300592"
    mc_ResBdbar        = 300593
    mc_ResBdbar_str    = "300593"
elif channel == "muon":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 6000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    sideband_L = 5000
    sideband_R = 5700
    mc_NonresBd        = 300700
    mc_NonresBd_str    = "300700"
    mc_NonresBdbar     = 300701
    mc_NonresBdbar_str = "300701"
    mc_ResBd           = 300702
    mc_ResBd_str       = "300702"
    mc_ResBdbar        = 300703
    mc_ResBdbar_str    = "300703"

config = toml.load('config.toml')
Storage = Utils.HDF5_IO(config['PATHS_{}'.format(channel)]['Storage'])
#safe_good_features = ['{}_y'.format(prefix),'Lxy_significance_over_B_chi2','{}_diMeson_vtx_y'.format(prefix),'{}_di{}_vtx_y'.format(prefix, lepton_big),'tracks_dEdx_diff','{}_z'.format(prefix),'diMeson_Kpi_piK_mass_avg','diMeson_Kpi_piK_mass_diff','angle_vtx_plane_mm_plane','angle_vtx_plane_{}_plane'.format(lepton_short_double)]
#safe_good_features = ['tracks_dEdx_diff', 'diMeson_Kpi_piK_mass_avg','diMeson_Kpi_piK_mass_diff','angle_vtx_plane_mm_plane','angle_vtx_plane_{}_plane'.format(lepton_short_double)]
safe_good_features = []

JPsi_mass = config["PDG"]["Jpsi_mass"]

Features_to_be_read = []
for k, v in config['features_{}'.format(channel)].items():
    Features_to_be_read.extend(v)
Features_to_be_read = Features_to_be_read + safe_good_features
features = config['features_{}'.format(channel)]['ML_features']
features = features + safe_good_features

# -------------------- Selection Criteria ------------------------------------------------------------------------------
if channel == "electron":
    main = Utils.selection_cuts()['main']
    select_ChargeSS = '(trackPlus_charge * trackMinus_charge > 0)'
    select_ChargeOS = '(trackPlus_charge * trackMinus_charge < 0)'
    select_q2_low = '( (diElectron_mass*diElectron_mass > 1.1e6) & (diElectron_mass*diElectron_mass <= 6e6) )'
    select_q2_high = '( (diElectron_mass*diElectron_mass > 6e6) & (diElectron_mass*diElectron_mass <= 11e6) )'
    select_b_mass_3000_6500 = "( ((B_mass) >= 3000) & ((B_mass) <= 6500))"
    select_mass_4000_5700_outside = "( ((B_mass) >= 5700) | ((B_mass) <= 4000) )"
    select_mass_4000_5700 = "( ((B_mass) < 5700) & ((B_mass) > 4000) )"
    select_CR = "((B_chi2 > 20) & (B_chi2 <= 30))"
    select_chi20 = "((B_chi2 < 20))"
elif channel == "muon":
    main = Utils.selection_cuts()['main']
    select_ChargeSS = '(trackPlus_charge * trackMinus_charge > 0)'
    select_ChargeOS = '(trackPlus_charge * trackMinus_charge < 0)'
    select_q2_low = '( (diMuon_mass*diMuon_mass > 1.1e6) & (diMuon_mass*diMuon_mass <= 6e6) )'
    select_q2_high = '( (diMuon_mass*diMuon_mass > 6e6) & (diMuon_mass*diMuon_mass <= 11e6) )'
    select_b_mass_3000_6500 = "( ((B_mass) >= 4700) & ((B_mass) <= 6000))"
    select_mass_4000_5700_outside = "( ((B_mass) >= 5700) | ((B_mass) <= 5000))"
    select_mass_4000_5700 = "( ((B_mass) < 5700) & ((B_mass) > 5000) )"
    select_CR = "((B_chi2 > 20) & (B_chi2 <= 30))"
    select_chi20 = "((B_chi2 < 20))"

# ----------------------------------------------------------------------------------------------------------------------
def FIT_branches_w_PCA(Data:pd.DataFrame,root_tag:str='BDT1') -> pd.DataFrame:
    random.seed(42)
    np.random.seed(42)

    _Data = Data.copy()

    branch_lepton = ['{}_pT'.format(lepton),'{}_eta'.format(lepton),'{}_phi'.format(lepton),'{}_iso_c40'.format(lepton)]
    branch_antilepton = ['{}_pT'.format(antilepton),'{}_eta'.format(antilepton),'{}_phi'.format(antilepton),'{}_iso_c40'.format(antilepton)]
    branch_trackPlus = ['trackPlus_pT','trackPlus_eta','trackPlus_phi','trackPlus_iso_c40']
    branch_trackMinus = ['trackMinus_pT','trackMinus_eta','trackMinus_phi','trackMinus_iso_c40']
    branch_dilepton = ['di{}_pvalue'.format(lepton_big), 'angle_pp_plane_{}_plane'.format(lepton_short_double)]
    branch_dimeson = ['diMeson_pvalue', 'angle_pp_plane_mm_plane']
    branch_b_meson_quality = ["B_chi2", "a0_significance",  "z0_significance",  "Lxy_significance", 'B_pT']
    branch_track1_track2 = ["dR_trackPlus_trackMinus", "diMeson_Kpi_mass", "diMeson_piK_mass", "tracks_dEdx_ratio"]
    branch_dilepton_dimeson = ["angle_{}_plane_mm_plane".format(lepton_short_double),"d_diMeson_di{}_significance".format(lepton_big)]
    branch_b_meson_dilepton = ["di{}_angle_alpha_sym".format(lepton_big), "di{}_angle_beta_sym".format(lepton_big), "d_B_di{}_significance".format(lepton_big)]
    branch_b_meson_dimeson= ["diMeson_angle_alpha_sym",  "diMeson_angle_beta_sym", "d_B_diMeson_significance"]

    #branch_combine_track1_track2_trackPlus_trackMinus = branch_trackPlus + branch_trackMinus + branch_track1_track2
    #branch_combine_dilepton_dimeson = branch_dilepton + branch_dilepton_dimeson + branch_dimeson
    #branch_combine_b_meson_dilepton = branch_b_meson_quality + branch_b_meson_dilepton + branch_dilepton
    #branch_combine_b_meson_dimeson = branch_b_meson_quality + branch_b_meson_dimeson + branch_dimeson

    Utils.fit_PCA_to_train(Storage,_Data[branch_lepton],n_components=1,root_tag=f'{root_tag}_branch_{lepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_antilepton],n_components=1,root_tag=f'{root_tag}_branch_{antilepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_trackPlus],n_components=1,root_tag=f'{root_tag}_branch_trackPlus')
    Utils.fit_PCA_to_train(Storage,_Data[branch_trackMinus],n_components=1,root_tag=f'{root_tag}_branch_trackMinus')
    Utils.fit_PCA_to_train(Storage,_Data[branch_dilepton],n_components=1,root_tag=f'{root_tag}_branch_di{lepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_dimeson],n_components=1,root_tag=f'{root_tag}_branch_dimeson')
    Utils.fit_PCA_to_train(Storage,_Data[branch_b_meson_quality],n_components=1,root_tag=f'{root_tag}_branch_b_meson_quality')
    Utils.fit_PCA_to_train(Storage,_Data[branch_track1_track2],n_components=1,root_tag=f'{root_tag}_branch_track1_track2')
    Utils.fit_PCA_to_train(Storage,_Data[branch_dilepton_dimeson],n_components=1,root_tag=f'{root_tag}_branch_di{lepton}_dimeson')
    Utils.fit_PCA_to_train(Storage,_Data[branch_b_meson_dilepton],n_components=1,root_tag=f'{root_tag}_branch_b_meson_di{lepton}')
    Utils.fit_PCA_to_train(Storage,_Data[branch_b_meson_dimeson],n_components=1,root_tag=f'{root_tag}_branch_b_meson_dimeson')

    #Utils.fit_PCA_to_train(Storage, _Data[branch_combine_track1_track2_trackPlus_trackMinus], n_components=1, root_tag=f'{root_tag}_branch_combine_track1_track2_trackPlus_trackMinus')
    #Utils.fit_PCA_to_train(Storage, _Data[branch_combine_dilepton_dimeson], n_components=1, root_tag=f'{root_tag}_branch_combine_dilepton_dimeson')
    #Utils.fit_PCA_to_train(Storage, _Data[branch_combine_b_meson_dilepton], n_components=1, root_tag=f'{root_tag}_branch_combine_b_meson_dilepton')
    #Utils.fit_PCA_to_train(Storage, _Data[branch_combine_b_meson_dimeson], n_components=1, root_tag=f'{root_tag}_branch_combine_b_meson_dimeson')


def TRANSFORM_branches_w_PCA(Data:pd.DataFrame,root_tag:str='BDT1') -> pd.DataFrame:
    random.seed(42)
    np.random.seed(42)

    _Data = Data.copy()

    branch_lepton = ['{}_pT'.format(lepton), '{}_eta'.format(lepton), '{}_phi'.format(lepton), '{}_iso_c40'.format(lepton)]
    branch_antilepton = ['{}_pT'.format(antilepton), '{}_eta'.format(antilepton), '{}_phi'.format(antilepton), '{}_iso_c40'.format(antilepton)]
    branch_trackPlus = ['trackPlus_pT', 'trackPlus_eta', 'trackPlus_phi', 'trackPlus_iso_c40']
    branch_trackMinus = ['trackMinus_pT', 'trackMinus_eta', 'trackMinus_phi', 'trackMinus_iso_c40']
    branch_dilepton = ['di{}_pvalue'.format(lepton_big), 'angle_pp_plane_{}_plane'.format(lepton_short_double)]
    branch_dimeson = ['diMeson_pvalue', 'angle_pp_plane_mm_plane']
    branch_b_meson_quality = ["B_chi2", "a0_significance", "z0_significance", "Lxy_significance", 'B_pT']
    branch_track1_track2 = ["dR_trackPlus_trackMinus", "diMeson_Kpi_mass", "diMeson_piK_mass", "tracks_dEdx_ratio"]
    branch_dilepton_dimeson = ["angle_{}_plane_mm_plane".format(lepton_short_double), "d_diMeson_di{}_significance".format(lepton_big)]
    branch_b_meson_dilepton = ["di{}_angle_alpha_sym".format(lepton_big), "di{}_angle_beta_sym".format(lepton_big), "d_B_di{}_significance".format(lepton_big)]
    branch_b_meson_dimeson = ["diMeson_angle_alpha_sym", "diMeson_angle_beta_sym", "d_B_diMeson_significance"]

    #branch_combine_track1_track2_trackPlus_trackMinus = branch_trackPlus + branch_trackMinus + branch_track1_track2
    #branch_combine_dilepton_dimeson = branch_dilepton + branch_dilepton_dimeson + branch_dimeson
    #branch_combine_b_meson_dilepton = branch_b_meson_quality + branch_b_meson_dilepton + branch_dilepton
    #branch_combine_b_meson_dimeson = branch_b_meson_quality + branch_b_meson_dimeson + branch_dimeson


    _Data['branch_{}'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_lepton],n_components=1,root_tag=f'{root_tag}_branch_{lepton}')
    _Data['branch_{}'.format(antilepton)] = Utils.PCA_transform(Storage,_Data[branch_antilepton], n_components=1,root_tag=f'{root_tag}_branch_{antilepton}')
    _Data['branch_trackPlus'] = Utils.PCA_transform(Storage,_Data[branch_trackPlus], n_components=1,root_tag=f'{root_tag}_branch_trackPlus')
    _Data['branch_trackMinus'] = Utils.PCA_transform(Storage,_Data[branch_trackMinus], n_components=1,root_tag=f'{root_tag}_branch_trackMinus')
    _Data['branch_di{}'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_dilepton], n_components=1,root_tag=f'{root_tag}_branch_di{lepton}')
    _Data['branch_dimeson'] = Utils.PCA_transform(Storage,_Data[branch_dimeson], n_components=1,root_tag=f'{root_tag}_branch_dimeson')
    _Data['branch_b_meson_quality'] = Utils.PCA_transform(Storage,_Data[branch_b_meson_quality], n_components=1,root_tag=f'{root_tag}_branch_b_meson_quality')
    _Data['branch_track1_track2'] = Utils.PCA_transform(Storage,_Data[branch_track1_track2], n_components=1,root_tag=f'{root_tag}_branch_track1_track2')
    _Data['branch_di{}_dimeson'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_dilepton_dimeson], n_components=1,root_tag=f'{root_tag}_branch_di{lepton}_dimeson')
    _Data['branch_b_meson_di{}'.format(lepton)] = Utils.PCA_transform(Storage,_Data[branch_b_meson_dilepton], n_components=1,root_tag=f'{root_tag}_branch_b_meson_di{lepton}')
    _Data['branch_b_meson_dimeson'] = Utils.PCA_transform(Storage,_Data[branch_b_meson_dimeson], n_components=1,root_tag=f'{root_tag}_branch_b_meson_dimeson')

    #_Data['branch_combine_track1_track2_trackPlus_trackMinus'] = Utils.PCA_transform(Storage, _Data[branch_combine_track1_track2_trackPlus_trackMinus], n_components=1, root_tag=f'{root_tag}_branch_combine_track1_track2_trackPlus_trackMinus')
    #_Data['branch_combine_dilepton_dimeson'] = Utils.PCA_transform(Storage, _Data[branch_combine_dilepton_dimeson], n_components=1, root_tag=f'{root_tag}_branch_combine_dilepton_dimeson')
    #_Data['branch_combine_b_meson_dilepton'] = Utils.PCA_transform(Storage, _Data[branch_combine_b_meson_dilepton], n_components=1, root_tag=f'{root_tag}_branch_combine_b_meson_dilepton')
    #_Data['branch_combine_b_meson_dimeson'] = Utils.PCA_transform(Storage, _Data[branch_combine_b_meson_dimeson], n_components=1, root_tag=f'{root_tag}_branch_combine_b_meson_dimeson')

    return _Data


def get_B_mass_true(df):
    print('Adding True Mass Column..')
    df["B_mass_true"] = ((df["info_is_true_nonres_Bd"]) | (df["info_is_true_res_Bd"])) * df["B_mass"] + ((df["info_is_true_nonres_BdBar"]) | (df["info_is_true_res_BdBar"])) * df["Bbar_mass"]
    return df


def predict_double(Storage, X_target=None):
    print('Adding Predictions..')
    X = X_target.copy()

    # ------- BDT1 ----------
    root_tag = 'BDT1'
    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()

    X_scaled = Utils.pd_scale(Storage, X[features], root_tag=root_tag)
    X_scaled = TRANSFORM_branches_w_PCA(Data=X_scaled, root_tag=root_tag)
    X_scaled = X_scaled[feat_idx]

    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}_Bkg"], X_target[f"P_{root_tag}_Bd"], X_target[f"P_{root_tag}_BdBar"], X_target[f"P_{root_tag}_sum"], X_target[f"P_{root_tag}_max"] = BDT_scores[:, 0], BDT_scores[:, 1], BDT_scores[:,2], BDT_scores[:,1:].sum(axis=1), BDT_scores[:, 1:].max(axis=1)

    # ------- BDT2 ----------
    root_tag = 'BDT2'
    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()

    X_scaled = Utils.pd_scale(Storage, X[features], root_tag=root_tag)
    X_scaled = TRANSFORM_branches_w_PCA(Data=X_scaled, root_tag=root_tag)
    X_scaled = X_scaled[feat_idx]
    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}_Bkg"], X_target[f"P_{root_tag}_Bd"], X_target[f"P_{root_tag}_BdBar"], X_target[f"P_{root_tag}_sum"], X_target[f"P_{root_tag}_max"] = BDT_scores[:, 0], BDT_scores[:, 1], BDT_scores[:,2], BDT_scores[:, 1:].sum(axis=1), BDT_scores[:, 1:].max(axis=1)

    X_target['P_BDT_max'] = np.max([X_target.P_BDT1_sum, X_target.P_BDT2_sum], axis=0)

    return X_target


def load_multiple_feather_files(file_list: list, selection: str, N_events: int, columns_to_read: list):
    print('Starts loading files..')
    files = []
    List_uniques_list = []

    for file_path in file_list:
        files.append(pd.read_feather(path=file_path, columns=columns_to_read, use_threads=True, storage_options=None))
    print('Done loading..')

    if selection == "None":
        for i in range(len(files)):
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]
    else:
        for i in range(len(files)):
            files[i] = files[i].query(selection)
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]

    print(f"Number of uniques: {[len(i) for i in List_uniques_list]}, total = {sum([len(i) for i in List_uniques_list])}")
    print(f"Length of files: {[len(i) for i in files]}, total = {sum([len(i) for i in files])}")

    df_files = pd.concat(files)
    df_files_w_true_mass = get_B_mass_true(df_files)
    df_files_w_true_mass_w_predictions = predict_double(Storage, df_files_w_true_mass)
    print('Done..')
    print()

    return df_files_w_true_mass_w_predictions

if channel == "electron":
    files_dictionary = {
        'MC_signal': ['/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300590_part_01.ftr', #Using Feather Produced by KFung(5/2023)
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300590_part_02.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300590_part_03.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300590_part_04.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300590_part_05.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300591_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300591_part_02.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300591_part_03.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300591_part_04.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300592_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v08_orig_trks/ntuple-300593_part_01.ftr'],
        'Data': sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if 'ntuple-data18_13TeV_periodL_part_0' in x]),
        'Bkg_MC' : ['/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300718_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300719_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300722_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300723_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300724_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300725_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300726_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300727_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300730_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300731_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300734_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300735_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300738_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300739_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300742_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300743_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300744_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300745_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300748_part_01.ftr',
                    '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_orig_trks/ntuple-300749_part_01.ftr',]
    }
elif channel == "muon":
    path = '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather_quality/'
    files_dictionary = {
        'MC_signal': [path + 'ntuple-300700_part_01.ftr',
                      path + 'ntuple-300700_part_02.ftr',
                      path + 'ntuple-300701_part_01.ftr',
                      path + 'ntuple-300701_part_02.ftr',
                      path + 'ntuple-300702_part_01.ftr',
                      path + 'ntuple-300703_part_01.ftr',
                      path + 'ntuple-300703_part_02.ftr'
                      ],
        'Data': [path + 'ntuple-data18_13TeV_periodK_part_01.ftr',
                 path + 'ntuple-data18_13TeV_periodK_part_02.ftr',
                 path + 'ntuple-data18_13TeV_periodK_part_03.ftr',
                 path + 'ntuple-data18_13TeV_periodK_part_04.ftr',
                 path + 'ntuple-data18_13TeV_periodK_part_05.ftr',
                 ],
            #sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if 'ntuple-data18_13TeV_periodK_part_7' in x]), #sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if 'ntuple-data18_13TeV_periodK_part_0' in x]), "/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon_channel_v0.9/feather/ntuple-data18_13TeV_periodK_part_03.ftr"
        'Bkg_MC': [ path + "ntuple-300720_part_01.ftr",
            path + "ntuple-300721_part_01.ftr",
            path + "ntuple-300728_part_01.ftr",
            path + "ntuple-300729_part_01.ftr",
            path + "ntuple-300732_part_01.ftr",
            path + "ntuple-300733_part_01.ftr",
            path + "ntuple-300736_part_01.ftr",
            path + "ntuple-300737_part_01.ftr",
            path + "ntuple-300740_part_01.ftr",
            path + "ntuple-300741_part_01.ftr",
            path + "ntuple-300746_part_01.ftr",
            path + "ntuple-300747_part_01.ftr"]
    }

MC_USR_highq2 = load_multiple_feather_files(
    file_list = files_dictionary['MC_signal'],
    selection = f"{main} & {select_ChargeOS} & {select_q2_high} & {select_b_mass_3000_6500} & {select_chi20}",
    N_events = -1,
    columns_to_read = Features_to_be_read)

print(MC_USR_highq2)

"""
DATA_SS_bkg = load_multiple_feather_files(
    file_list = files_dictionary['Data'], # files_dictionary['Data']
    selection = f"{main} & {select_ChargeSS} & {select_q2_high} & {select_b_mass_3000_6500}", 
    N_events = -1,
    columns_to_read = Features_to_be_read)

print(DATA_SS_bkg)

Data_USR_highq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'], 
    selection = Utils.selection_cuts()['usr_highq2'], 
    N_events = -1,
    columns_to_read = Features_to_be_read)

print(Data_USR_highq2)
"""

# Library --------------------------------------------------------------------------------------------------------------

def selections_predicter(input_data, THRESHOLD1: float = 0.5, THRESHOLD2: float = 0.5):
    PData = input_data.copy()
    # Global selection:
    if all(PData.info_sample != 'data'):
        PData = PData[(PData.P_BDT1_sum > THRESHOLD1) & (PData.P_BDT2_sum > THRESHOLD2)]
    else:
        PData = PData[(PData.P_BDT1_sum > THRESHOLD1) & (PData.P_BDT2_sum > THRESHOLD2)]

    # Local selection:
    PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()

    # Mass Hypothesis:
    PData['mass_hypotesis'] = 'NaN'
    max_select = np.argmax([PData.P_BDT1_sum, PData.P_BDT2_sum], axis=0)
    max_select_BDT1 = np.argmax([PData.P_BDT1_Bd, PData.P_BDT1_BdBar], axis=0)
    max_select_BDT2 = np.argmax([PData.P_BDT2_Bd, PData.P_BDT2_BdBar], axis=0)
    PData['mass_hypotesis'][(max_select == 0) & (max_select_BDT1 == 0)] = 'Bd'
    PData['mass_hypotesis'][(max_select == 0) & (max_select_BDT1 == 1)] = 'BdBar'
    PData['mass_hypotesis'][(max_select == 1) & (max_select_BDT2 == 0)] = 'Bd'
    PData['mass_hypotesis'][(max_select == 1) & (max_select_BDT2 == 1)] = 'BdBar'

    def Apply_Mass_to_plot(df):
        df['B_mass_to_plot'] = np.zeros(len(df.B_mass)) #B_mass, constrain_Bmass
        df['B_mass_to_plot'][(df.mass_hypotesis == 'BdBar')] = df.Bbar_mass #constrain_Bbarmass
        df['B_mass_to_plot'][(df.mass_hypotesis == 'Bd')] = df.B_mass #constrain_Bmass
        return df

    PData = Apply_Mass_to_plot(PData)

    return PData

def select_for_fitting(settings_input: tuple = ('Res', 'Bd'), Threshold1: list = None, Threshold2: list = None) -> None:
    
    assert settings_input[0] == ('Res' or 'NonRes'), "First entry in settings tuple needs top be either 'Res' or 'NonRes'."
    assert settings_input[1] == ('Bd' or 'BdBar'), "Second entry in settings tuple needs top be either 'Bd' or 'BdBar'."

    if settings_input == ('Res', 'Bd'):
        info_sample = mc_ResBd_str
    if settings_input == ('NonRes', 'Bd'):
        info_sample = mc_NonresBd_str
    if settings_input == ('Res', 'BdBar'):
        info_sample = mc_ResBdbar_str
    if settings_input == ('NonRes', 'BdBar'):
        info_sample = mc_NonresBdbar_str

    For_plotting = pd.DataFrame()
    for i in Threshold1:
        for j in Threshold2:
            Data_mass = selections_predicter(MC_USR_highq2, i, j)
            select = (Data_mass.info_sample.isin(['300702'])) | (Data_mass.info_sample.isin(['300703'])) #(Data_mass.info_sample.isin(['data'])) #(Data_mass.info_sample.isin(['300700'])) | (Data_mass.info_sample.isin(['300701']))
            selected_Data = Data_mass[select].B_mass_to_plot
            savestring = settings_input[0] + settings_input[1]
            multiple_cuts = pd.DataFrame([selected_Data.values]).T.rename(columns={0: f"{savestring}Data_{i},{j}"})
            For_plotting = pd.concat([For_plotting, multiple_cuts], axis=1)

    return For_plotting

THRESHOLD1_list = [0.1,0.5,0.7,0.9,0.95] # 0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.925,0.95,0.975,0.99
THRESHOLD2_list = [0.1,0.5,0.7,0.9,0.95]

selected_Data_Punzi = select_for_fitting(('Res', 'Bd'), THRESHOLD1_list, THRESHOLD2_list)
selected_Data_Punzi.to_feather('MC_USR_highq2_threshold.ftr')
print(selected_Data_Punzi)