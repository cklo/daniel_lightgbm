import mplhep as hep

# Imports
import sys
sys.path.append("..")
import pandas as pd
import numpy as np
from Utils_Store import Utils
import toml
from sklearn import preprocessing, model_selection, metrics
import seaborn as sns
import random


import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
#plt.style.use("seaborn-v0_8-whitegrid")
from scipy.special import logit, expit, erf
import os

import optuna
random.seed(42)
from iminuit import Minuit
from scipy import stats
mpl.rcParams.update({
    'text.usetex': False,
    'font.family': 'stixgeneral',
    'mathtext.fontset': 'stix',
    })

import tables
import h5py

import scipy.integrate as integrate
import numpy as np
import ROOT
from array import array
from ROOT import RooFitResult
import pickle
from statistics import mean

#--------------------------------------------------------
channel = "muon"
#--------------------------------------------------------

# Prefix and abbreviations
# =========
if channel == "muon":
    tree_name    = "Nominal/BaseSelection_KStarMuMu_BmumuKstSelection"
    prefix       = "BmumuKst"
    lepton       = "muon"
    lepton_big   = "Muon"
    lepton_short = "mu"
    lepton_short_double = "mumu"
    antilepton   = "antimuon"
elif channel == "electron":
    tree_name    = "Nominal/BaseSelection_KStaree_BeeKstSelection"
    prefix       = "BeeKst"
    lepton       = "electron"
    lepton_big   = "Electron"
    lepton_short = "e"
    lepton_short_double = "ee"
    antilepton   = "positron"
else:
    print ('"channel" should be either "electron" or "muon"')
    quit()

# Pre-selection cuts
# ============
if channel == "electron":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 5000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    GSF_pT_low         = 5000
    GSF_eta_limit      = 2.5
    sideband_L = 4000
    sideband_R = 5700
    mc_NonresBd        = 300590
    mc_NonresBd_str    = "300590"
    mc_NonresBdbar     = 300591
    mc_NonresBdbar_str = "300591"
    mc_ResBd           = 300592
    mc_ResBd_str       = "300592"
    mc_ResBdbar        = 300593
    mc_ResBdbar_str    = "300593"
elif channel == "muon":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 6000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    sideband_L = 5000
    sideband_R = 5700
    mc_NonresBd        = 300700
    mc_NonresBd_str    = "300700"
    mc_NonresBdbar     = 300701
    mc_NonresBdbar_str = "300701"
    mc_ResBd           = 300702
    mc_ResBd_str       = "300702"
    mc_ResBdbar        = 300703
    mc_ResBdbar_str    = "300703"

#-----------------------Accessing Root files---------------------------------
def createCanvasPads():
    c1 =  ROOT.TCanvas('B_Vertex_Mass','Events', 800, 800)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0) #xmin, ymin, xmax, ymax
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c1.cd()  # returns to main canvas before defining pad2
    pad2 = ROOT.TPad("B_Vertex_Mass", "B_Vertex_Mass", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    pad2.SetGridx()
    pad2.Draw()

    return c1, pad1, pad2
c1, pad1, pad2 = createCanvasPads()

def SidebandPlotter(channel, fitting_region):
    #-------------------------defining ROOT variables--------------------
    tree = pd.read_feather("MC_USR_highq2_threshold.ftr") # Input your BDT-predicted threshold feather files here

    attr_names = ["ResBdData_0.7,0.7"] # Choose your threshold here, for example ResBdData_0.9,0.9 is BDT1, BDT2 = 0.9, 0.9 threshold

    mass = np.zeros(1,dtype=float)
    #numEntry = tree.GetEntries()
    if channel == "electron":
        LC1, LC2, HC1, HC2 = 3500, 4000, 5700, 6500
    elif channel == "muon":
        LC1, LC2, HC1, HC2 = 4700, 5000, 5700, 6000 # 4700, 5000, 5700, 6000
    w = ROOT.RooWorkspace("w")
    m = ROOT.RooRealVar('m', 'm', LC1, HC2) # LC2, HC1

    m_arg = ROOT.RooArgSet(m)
    w.Import(m_arg)
    data = ROOT.RooDataSet("data", "data", m_arg)

    data.Print("v")
    #-------------------------Accessing data-----------------
    n_events = len(tree)
    print(n_events)

    counter = 0
    for i in range(n_events):
        mass = tree[attr_names[0]].values[i] # access data
        if ((LC1 < mass< LC2 or HC1 < mass< HC2) and fitting_region == "sideband") or (LC2 < mass < HC1 and fitting_region == "All_region"):
            ROOT.RooAbsRealLValue.__assign__(m, mass)
            data.add(m_arg,1.0)
            counter+=1
    print(counter)
    data.Print("v")
    filename = ""
    # -----------------Fitting---------------------
    if fitting_region == "sideband":
        m = w.var('m')
        m.setRange("LSB", LC1, LC2)
        m.setRange("HSB", HC1, HC2)
        w.factory("RooExponential:model(m, c1[-35e-5, -1e-1, 1e-6])")
        #w.factory("RooExponential:exp_pdf(m, c1[-35e-5, -1e-1, 1e-6])")
        #w.factory("RooBukinPdf:bkg_pdf(m, Xp[5450,5350,5550], sigp[200,0,20000], xi[-0.5,-1,1],rho1[-0.5,-5,5],rho2[0.5,-5,5])") #Xp[5450,5350,5550]
        #w.factory(f"SUM:model(No_Exp[150,0,{counter}]*exp_pdf, No_Bukin[20,0,{counter}]*bkg_pdf)")
        pdf = w.pdf('model')
        pdf.fitTo(data, ROOT.RooFit.PrintLevel(-1),  ROOT.RooFit.Range("LSB,HSB"))#, ROOT.RooFit.Range("3500,6500"))
        normSet = ROOT.RooFit.NormSet(m_arg)
        m.setRange("signal", LC2, HC1)
        reg2 = pdf.createIntegral(m_arg,normSet,ROOT.RooFit.Range("signal")).getVal()
        reg1 = pdf.createIntegral(m_arg,normSet,ROOT.RooFit.Range("LSB")).getVal()
        reg3 = pdf.createIntegral(m_arg,normSet,ROOT.RooFit.Range("HSB")).getVal()
        print(reg1,reg2,reg3)
        print(reg2*counter)
        #------------------Create pads for plots--------------
        #------------------plotting--------------------
        c1, pad1, pad2 = createCanvasPads()
        pad1.cd()  #upper pad
        pl = m.frame(ROOT.RooFit.Title("B_Meson_Mass"))
        data.plotOn(pl)
        pdf.plotOn(pl, ROOT.RooFit.Range(LC1, LC2), ROOT.RooFit.LineStyle(1))
        pdf.plotOn(pl, ROOT.RooFit.Range(HC1, HC2), ROOT.RooFit.LineStyle(1))
        pdf.plotOn(pl, ROOT.RooFit.Range(LC2, HC1), ROOT.RooFit.LineStyle(2))
        pdf.paramOn(pl, ROOT.RooFit.Layout(0.4,0.6,0.9))
        pdf.plotOn(pl)
        pl.GetXaxis().SetRangeUser(LC1, HC2)
        pl.SetMinimum(0.01)
        pl.SetTitle("")
        pl.Draw()  #draw on the current pad
        pad2.cd()  #lowerpad

        pullhist = pl.pullHist()
        pl2 = m.frame(ROOT.RooFit.Title(""))
        pl2.addPlotable(pullhist, "P")

        pl2.GetXaxis().SetNdivisions(207)
        pl2.GetYaxis().SetNdivisions(207)
        pl2.GetYaxis().SetLabelSize(0.07)

        pl2.GetYaxis().SetRangeUser(-5., 5.)
        pl2.GetXaxis().SetTitleSize(0.10)
        pl2.GetXaxis().SetLabelSize(0.07)

        pl2.SetTitle("")
        pl2.GetXaxis().SetTitle("B_Vertex_Mass [MeV]")
        pl2.GetYaxis().SetTitle("Pull")
        pl2.GetYaxis().SetTitleSize(0.09)
        pl2.GetYaxis().SetTitleOffset(0.3)

        pl2.Draw()

        line = ROOT.TLine(LC1,0,HC2,0)
        line.SetLineColor(ROOT.kRed)
        line.SetLineWidth(2)
        line.Draw("same")

        c1.Draw()
        c1.SaveAs("Testing_Roofit.png")

    elif fitting_region == "All_region":
        m = w.var('m')
        m.setRange("Overall", LC2, HC1) # LC2, HC1
        w.factory("RooCrystalBall::DCB_pdf(m, mu[5280,5200,5300], width1[71.7,50,90], width2[65.699,50,90], a1[0.9256,0.5,2], p1[7.71,1,150], a2[0.700028,0.5,2], p2[4.190,1,150])")  # , alphaR[2,0,10], nR[5,0,500])")
        w.factory("RooExponential:exp_pdf(m, c1[-2e-4, -1e-2, 1e-10])")
        w.factory("RooBukinPdf:bplus_pdf(m, Xp[5500,5400,5600], sigp[100,30,150],xi[-0.5,-1,1],rho1[-0.5,-5,5],rho2[0.5,-10,10])")
        w.factory("SUM:bkg_pdf(No_Exp[800,0,500000]*exp_pdf, No_Bplus[800,0,500000]*bplus_pdf)")
        w.factory("SUM:model(No_sig[15000,0,500000]*DCB_pdf, No_bkg[5000,0,500000]*bkg_pdf)")
        pdf = w.pdf('model')
        pdf.fitTo(data, ROOT.RooFit.PrintLevel(-1), ROOT.RooFit.Range("Overall"))  # , ROOT.RooFit.Range("4700,5700"))
        # ------------------Create pads for plots--------------
        # ------------------plotting--------------------
        c1, pad1, pad2 = createCanvasPads()
        pad1.cd()  # upper pad
        pl = m.frame(ROOT.RooFit.Title("B_Meson_Mass"))
        data.plotOn(pl)
        pdf.plotOn(pl, ROOT.RooFit.Components("exp_pdf"), ROOT.RooFit.LineStyle(2), ROOT.RooFit.LineColor(1))
        pdf.plotOn(pl, ROOT.RooFit.Components("bplus_pdf"), ROOT.RooFit.LineColor(1), ROOT.RooFit.LineStyle(2))
        pdf.plotOn(pl, ROOT.RooFit.Components("DCB_pdf"), ROOT.RooFit.LineStyle(2), ROOT.RooFit.LineColor(4))
        pdf.plotOn(pl, ROOT.RooFit.Components("model"), ROOT.RooFit.LineColor(3), ROOT.RooFit.LineStyle(2))

        pdf.paramOn(pl, ROOT.RooFit.Layout(0.7, 0.9, 0.9))
        pdf.plotOn(pl)
        chi_square = pl.chiSquare(13)  # Number of free parameter in your fitting
        print(chi_square)
        legend = ROOT.TLegend(0.15, 0.15, 0.25, 0.25)  # Position (xmin,ymin,xmax,ymax)
        legend.SetBorderSize(0)  # no border
        legend.SetFillStyle(0)  # transparent
        legend.SetTextSize(.05)
        legend.AddEntry(pl, '#chi^{2}' + ' / ndf = {:.3f}'.format(chi_square), '')
        pl.GetXaxis().SetRangeUser(LC2, HC1) # LC2, HC1
        # pl.GetYaxis().SetRangeUser(0., 1500.)
        pl.SetMinimum(0.01)
        # pl.GetYaxis().SetRangeUser(0.01, 1000.)
        pl.SetTitle("")
        pl.Draw()  # draw on the current pad
        legend.Draw()
        pad2.cd()  # lowerpad

        pullhist = pl.pullHist()
        pl2 = m.frame(ROOT.RooFit.Title(""))
        pl2.addPlotable(pullhist, "P")

        pl2.GetXaxis().SetNdivisions(207)
        pl2.GetYaxis().SetNdivisions(207)
        pl2.GetYaxis().SetLabelSize(0.07)

        pl2.GetYaxis().SetRangeUser(-5., 5.)
        pl2.GetXaxis().SetTitleSize(0.10)
        pl2.GetXaxis().SetLabelSize(0.07)

        pl2.SetTitle("")
        pl2.GetXaxis().SetTitle("B_Vertex_Mass [MeV]")
        pl2.GetYaxis().SetTitle("Pull")
        pl2.GetYaxis().SetTitleSize(0.09)
        pl2.GetYaxis().SetTitleOffset(0.3)

        pl2.Draw()

        line = ROOT.TLine(LC2, 0, HC1, 0) # LC2, 0, HC1, 0
        line.SetLineColor(ROOT.kRed)
        line.SetLineWidth(2)
        line.Draw("same")

        c1.Draw()
        c1.SaveAs("Try_RooFit.png")
    print("Finished fitting")

SidebandPlotter(channel, "sideband")