Welcome to the LightGBM models for the RK* analysis. It was developed by Daniel Hans Munk, University of Copenhagen (2023).
Based on the Daniel branch, this branch is synchronized with both the electron and muon channels and can be run on lxplus platforms.

# [Documentation]

The documentation of the LightGBM model lives here: https://gitlab.cern.ch/dmunk/rkstar_masterthesis/-/tree/master/
The documentation of the RK* Analysis lives here: https://gitlab.cern.ch/RKstar

## Installation on lxplus 7 (to be adapted for other computer systems)
```bash
setupATLAS
lsetup "views LCG_101_ATLAS_14 x86_64-centos7-gcc8-opt"
git clone https://:@gitlab.cern.ch:8443/cklo/boom_cp.git
cd 2BDT_w_extra_features
python -m pip install --user -r requirements.txt
```
# [Important analysis reference]
```bash
Recent BDT_Presentation by Daniel (https://indico.cern.ch/event/1280538/contributions/5379645/attachments/2635917/4560196/RK_star_update26042023.pdf)
```