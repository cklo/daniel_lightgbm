# Imports
import sys
sys.path.append("..")
import pandas as pd
import numpy as np
from Utils_Store import Utils
import plot_utils as plotting
import toml
from sklearn import preprocessing, model_selection, metrics, decomposition
import seaborn as sns
import random

import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
#plt.style.use("seaborn-v0_8-whitegrid")
from scipy.special import logit, expit
import os

from sklearn.preprocessing import MinMaxScaler, StandardScaler, LabelBinarizer
#import verstack
import Verstack_cluster_safe.LGBMTuner as verstack
import optuna
import sklearn.inspection
from sklearn.base import BaseEstimator
random.seed(42)

mpl.rcParams.update({
    'text.usetex': False,
    'font.family': 'stixgeneral',
    'mathtext.fontset': 'stix',
    })

#--------------------------------------------------------
channel = "electron"
#--------------------------------------------------------

# Prefix and abbreviations
# =========
if channel == "muon":
    tree_name    = "Nominal/BaseSelection_KStarMuMu_BmumuKstSelection"
    prefix       = "BmumuKst"
    lepton       = "muon"
    lepton_big   = "Muon"
    lepton_short = "mu"
    lepton_short_double = "mumu"
    antilepton   = "antimuon"
elif channel == "electron":
    tree_name    = "Nominal/BaseSelection_KStaree_BeeKstSelection"
    prefix       = "BeeKst"
    lepton       = "electron"
    lepton_big   = "Electron"
    lepton_short = "e"
    lepton_short_double = "ee"
    antilepton   = "positron"
else:
    print ('"channel" should be either "electron" or "muon"')
    quit()

# Pre-selection cuts
# ============
if channel == "electron":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 5000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    GSF_pT_low         = 5000
    GSF_eta_limit      = 2.5
    sideband_L = 4000
    sideband_R = 5700
    mc_NonresBd        = 300590
    mc_NonresBd_str    = "300590"
    mc_NonresBdbar     = 300591
    mc_NonresBdbar_str = "300591"
    mc_ResBd           = 300592
    mc_ResBd_str       = "300592"
    mc_ResBdbar        = 300593
    mc_ResBdbar_str    = "300593"
elif channel == "muon":
    B_mass_low         = 3000
    B_mass_up          = 6500
    kaonPion_mass_low  = 690
    kaonPion_mass_up   = 1110
    pionKaon_mass_low  = 690
    pionKaon_mass_up   = 1110
    dR_low             = 0.1
    lepton_pT_low      = 6000
    lepton_eta_limit   = 2.5
    meson_pT_low       = 500
    meson_eta_limit    = 2.5
    diLepton_mass_up   = 7000
    InDet_pT_low       = 500
    InDet_eta_limit    = 2.5
    sideband_L = 5000
    sideband_R = 5700
    mc_NonresBd        = 300700
    mc_NonresBd_str    = "300700"
    mc_NonresBdbar     = 300701
    mc_NonresBdbar_str = "300701"
    mc_ResBd           = 300702
    mc_ResBd_str       = "300702"
    mc_ResBdbar        = 300703
    mc_ResBdbar_str    = "300703"

# Loading config -------------------------------------------------------------------------------------------------------

config = toml.load('config.toml')
Storage = Utils.HDF5_IO(config['PATHS_{}'.format(channel)]['Storage'])

Features_to_be_read = []
for k, v in config['features_{}'.format(channel)].items():
    Features_to_be_read.extend(v)
features = config['features_{}'.format(channel)]['ML_features']

# Loading Data ---------------------------------------------------------------------------------------------------------

def load_multiple_feather_files(file_list:list, selection:str, N_events:int, columns_to_read:list):
    print('Starts loading files..')
    files = []
    List_uniques_list = []
    for file_path in file_list:
        files.append(pd.read_feather(path=file_path, columns=columns_to_read, use_threads=True, storage_options=None))
    print('Done loading..')

    if selection == "None":
        for i in range(len(files)):
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]
    else:
        for i in range(len(files)):
            files[i] = files[i].query(selection)
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]

    print(f"Number of uniques: {[len(i) for i in List_uniques_list]}, total = {sum([len(i) for i in List_uniques_list])}")
    print(f"Length of files: {[len(i) for i in files]}, total = {sum([len(i) for i in files])}")

    df_files = pd.concat(files)
    print('Done..')
    print()

    return df_files

if channel == "electron":
    files_dictionary = {
        'MC_signal': ['/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300590_part_01.ftr', # Using latest refit Feather Produced by KFung (6/2023)
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300590_part_02.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300590_part_03.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300590_part_04.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300591_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300591_part_02.ftr'],
        'Data': sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if 'ntuple-data18_13TeV_periodK_part_0' in x]), # Using 10 feathers (PeriodK) for testing
        }
elif channel == "muon":
    files_dictionary = {
        'MC_signal': ['/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300700_part_01.ftr', # Using latest refit Feather Produced by KFung (6/2023)
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300700_part_02.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300701_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300701_part_02.ftr',],
        'Data': sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if 'ntuple-data18_13TeV_periodK_part_0' in x]),  # Using 10 feathers (PeriodK) for testing
    }

MC_USR_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['MC_signal'], # ["/eos/home-c/cklo/LightGBM/data/Storages/MC_USR_lowq2_muon_PCA_ExtraFeatures.ftr"]
    selection = Utils.selection_cuts()['usr_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)
print(MC_USR_lowq2)
#MC_USR_lowq2.reset_index().to_feather("/eos/home-c/cklo/LightGBM/data/Storages/MC_USR_lowq2_muon_PCA_ExtraFeatures.ftr")

NN1trb_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'], # ['/eos/home-c/cklo/LightGBM/data/Storages/NN1trb_lowq2_muon_PCA_ExtraFeatures.ftr']
    selection = Utils.selection_cuts()['nn1trb_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)
print(NN1trb_lowq2)
#NN1trb_lowq2.reset_index().to_feather("/eos/home-c/cklo/LightGBM/data/Storages/NN1trb_lowq2_muon_PCA_ExtraFeatures.ftr")
# Select 6 best Candidates among events
NN1trb_lowq2 = NN1trb_lowq2.assign(Lxy_significance_over_B_chi2=lambda x: x['Lxy_significance'] / x['B_chi2']).sort_values("Lxy_significance_over_B_chi2").groupby("info_event_number").tail(6).sort_index()
print(NN1trb_lowq2)

NN2trb_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'], # ["/eos/home-c/cklo/LightGBM/data/Storages/NN2trb_lowq2_muon_PCA_ExtraFeatures.ftr"]
    selection = Utils.selection_cuts()['nn2trb_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)
print(NN2trb_lowq2)
#NN2trb_lowq2.reset_index().to_feather("/eos/home-c/cklo/LightGBM/data/Storages/NN2trb_lowq2_muon_PCA_ExtraFeatures.ftr")
# Select 6 best Candidates among events
NN2trb_lowq2 = NN2trb_lowq2.assign(Lxy_significance_over_B_chi2=lambda x: x['Lxy_significance'] / x['B_chi2']).sort_values("Lxy_significance_over_B_chi2").groupby("info_event_number").tail(6).sort_index()
print(NN2trb_lowq2)

def Data_loader():
    print('Loading MC signal..')
    USR = MC_USR_lowq2
    USR.drop(USR[~((USR.info_is_true_nonres_Bd == True) | (USR.info_is_true_nonres_BdBar == True))].index, inplace=True)
    USR.drop(USR[(USR.info_is_true_nonres_Bd == True)].index[config['ML_config']['N_Signal']['Bd']:], inplace=True)
    USR.drop(USR[(USR.info_is_true_nonres_BdBar == True)].index[config['ML_config']['N_Signal']['BdBar']:], inplace=True)

    label = (USR.info_sample == 0) * 0
    for i in config['labels_{} Sig_VS_Bkg'.format(channel)]:
        label += (USR.info_sample == str(i)) * config['labels_{} Sig_VS_Bkg'.format(channel)][str(i)]
    USR['label'] = label

    print('Loading background sidebands..')
    SB = NN1trb_lowq2
    SB.drop(SB.index[config['ML_config']['N_background']:], inplace=True)
    label = (SB.info_sample == 0) * 0
    for i in config['labels_{} Sig_VS_Bkg'.format(channel)]:
        label += (SB.info_sample == str(i)) * config['labels_{} Sig_VS_Bkg'.format(channel)][str(i)]
    SB['label'] = label

    print('Loading background signal region..')
    SR = NN2trb_lowq2
    SR.drop(SR.index[config['ML_config']['N_background']:], inplace=True)
    label = (SR.info_sample == 0) * 0
    for i in config['labels_{} Sig_VS_Bkg'.format(channel)]:
        label += (SR.info_sample == str(i)) * config['labels_{} Sig_VS_Bkg'.format(channel)][str(i)]
    SR['label'] = label

    print("Data_loader Finished")
    return USR, SB, SR

USR, SB, SR = Data_loader()

# plotting input features ----------------------------------------------------------------------------------------------

def plot_pre_processed_data(data: list = [], labels: list = [], n_cols: int = 5):
    assert len(data) > 0, 'Data list is empty.'
    assert len(labels) == len(data), 'label list and data list is not equal.'
    assert np.all([item.shape[1] for item in data]), 'All elements in data list has to have same amount of columns.'
    for i in range(len(data)):
        assert list(data[0].columns) == list(data[i].columns), 'Not all columns are equal'

    fig = plt.figure(figsize=(20, 25), dpi=300, tight_layout=True)
    # fig = plt.figure(figsize=(12,5),dpi=300, tight_layout=True)

    fig.suptitle('Input features (normalized)', fontsize=25)

    feature_names = list(data[0].columns)

    nrows = data[0].shape[1] // n_cols + (data[0].shape[1] % n_cols > 0)

    for i in range(data[0].shape[1]):

        # add a new subplot iteratively using nrows and cols
        ax = plt.subplot(nrows, n_cols, i + 1)

        # filter df and plot ticker on the new subplot axis

        plot_bottom = np.array([np.percentile(item.iloc[:, i], 1) for item in data])
        plot_top = np.array([np.percentile(item.iloc[:, i], 99) for item in data])
        ypad = 0.03 * (plot_top - plot_bottom)

        min_range = np.min(plot_bottom) - np.max(ypad)
        max_range = np.max(plot_top) + np.min(ypad)
        Bin = 100
        bindwidth = (max_range - min_range) / Bin

        bindwidth_str = str(float(f"{bindwidth:.1g}")).split('.')[0] if float(str(float(f"{bindwidth:.1g}")).split('.')[-1]) == 0 else str(float(f"{bindwidth:.1g}"))

        plot = [item.iloc[:, i] for item in data]
        for j in range(len(plot)):
            ax.hist(plot[j], bins=Bin, range=(min_range, max_range), alpha=0.5, label=labels[j], density=True)

        ax.set_ylabel(f"Density / ({bindwidth_str})", loc='top', size=12)

        ax.set(yscale='log')
        # ax.set(yticks=[],xticks=[])
        ax.set_xlabel(feature_names[i], fontsize=15, labelpad=0.8)
        ax.tick_params(axis='both', which='major', labelsize=12)
        ax.tick_params(axis='both', which='minor', labelsize=12)
        # ax.yaxis.set_major_locator(mpl.ticker.LogLocator(numticks=5))

    plt.tight_layout(pad=0.2)
    fig.subplots_adjust(top=0.96, bottom=0.02)

    # ax.legend(bbox_to_anchor=(0.45,-0.05),    #bbox_transform=fig.transFigure, mode="expand", ncol=1, fontsize = 20)
    ax.legend(bbox_to_anchor=(0.5, 0.0), bbox_transform=fig.transFigure, mode="expand", ncol=1, fontsize=20)

    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "X_train_features.pdf", bbox_inches='tight', dpi=300)

    plt.show()

plot_pre_processed_data(data=[USR[features], SB[features], SR[features]], labels=['Signal Region (SR)', 'Sideband1 (SB1)', 'Sideband1 (SB2)'])

cpu_count = int(np.where(os.cpu_count() < 0, 1, os.cpu_count()))
cpu_count

# Training GBDT1 -------------------------------------------------------------------------------------------------------

BDT1 = pd.concat([USR,SB])

def train_test_split_stratified(X):
    ratios = {
        'train': 0.7,
        'valid': 0.0,
        'test': 0.30}
    np.random.seed(42)
    X_train, X_rem, y_train, y_rem = model_selection.train_test_split(X, X.label, train_size=ratios['train'],stratify=X.label, shuffle=True,random_state=42)
    if ratios['valid'] == 0:
        X_test = X_rem
        y_test = y_rem
        return {'X_train': X_train, 'y_train': y_train, 'X_test': X_test, 'y_test': y_test}
    else:
        X_valid, X_test, y_valid, y_test = model_selection.train_test_split(X_rem, y_rem, test_size=(ratios['test']/(1-ratios['train'])),stratify=y_rem,shuffle=True,random_state=42)
        return {'X_train': X_train, 'y_train': y_train, 'X_valid': X_valid, 'y_valid': y_valid, 'X_test': X_test, 'y_test': y_test}

splits = train_test_split_stratified(BDT1)

Utils.fit_scaler_to_train(Storage,splits['X_train'][features],root_tag='BDT1')
X_train1_scaled = Utils.pd_scale(Storage,splits['X_train'][features], root_tag= 'BDT1', target_tag = 'X_train')
X_test1_scaled = Utils.pd_scale(Storage,splits['X_test'][features], root_tag= 'BDT1', target_tag = 'X_test')

def Run_tuner(Storage=Storage, root_tag='BDT1'):
    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    print('Start with training model GBDT1')
    np.random.seed(42)
    # print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric='log_loss', trials=100, visualization=False, seed=42, verbosity=1, n_jobs=35)

    tuner.fit(X=X_train1_scaled, y=splits['y_train'].astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df=tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state', 'intermediate_values'), multi_index=True), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df=pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0: 'value'}), overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model", tuner=tuner, feature_names=list(X_train1_scaled.columns), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/best_params", df=pd.DataFrame([tuner.best_params]).T, overwrite=True)

    print('Done with training model')

Run_tuner()

def Verstack_Optimization_plots(root_tag:str=None):
    P = plotting.Verstack_train_plots()
    fig = plt.figure(figsize=(11,3),dpi=300)
    P.plot_optimization_history(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=131)
    P.plot_param_imp(Storage.read_pd(key=f"{root_tag}/tuner_param_imp"),fig=fig,placement=132)
    P.plot_intermediate(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=133)
    plt.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save']+"BDT1_optimization",bbox_inches = 'tight',dpi=300)
    plt.show()

Verstack_Optimization_plots(root_tag='BDT1')

Storage.read_pd(key=f"{'BDT1'}/best_params")

def Predict(Storage, X_target=None, root_tag: str = 'BDT1'):
    print("predict start")
    X = X_target.copy()

    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()
    X=X[feat_idx]

    X_scaled = Utils.pd_scale(Storage, X, root_tag=root_tag)
    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}"] = BDT_scores

    return X_target

X_test_BDT1 = Predict(Storage, splits['X_test'], root_tag='BDT1')

def BDT1_response_in_USR__NN1TRB_testset():
    P = plotting.plot_response(input_data=X_test_BDT1, threshold=0.7, root_tag='BDT1', test_data=True)

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0.95, 1))
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224)
    main_title = "GBDT1 Response in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}1^{\mathrm{data}}\}$ test set, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT1_test", bbox_inches='tight', dpi=300)

    plt.show()

BDT1_response_in_USR__NN1TRB_testset()

# Training GBDT2 -------------------------------------------------------------------------------------------------------

BDT2 = pd.concat([USR,SR])

splits_BDT2 = train_test_split_stratified(BDT2)
Utils.fit_scaler_to_train(Storage,splits_BDT2['X_train'][features],root_tag='BDT2')
X_train2_scaled = Utils.pd_scale(Storage,splits_BDT2['X_train'][features], root_tag= 'BDT2', target_tag = 'X_train')
X_test2_scaled = Utils.pd_scale(Storage,splits_BDT2['X_test'][features], root_tag= 'BDT2', target_tag = 'X_test')


def Run_tuner(Storage=Storage, root_tag='BDT2'):
    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    print('Start with training model GBDT2')
    np.random.seed(42)
    # print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric='log_loss', trials=100, visualization=False, seed=42, verbosity=1, n_jobs=35)

    tuner.fit(X=X_train2_scaled, y=splits_BDT2['y_train'].astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df=tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state', 'intermediate_values'), multi_index=True), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df=pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0: 'value'}), overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model", tuner=tuner, feature_names=list(X_train2_scaled.columns), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/best_params", df=pd.DataFrame([tuner.best_params]).T, overwrite=True)

    print('Done with training model')

    return tuner

BDT2_tuner = Run_tuner()

def Verstack_Optimization_plots(root_tag:str=None):
    P = plotting.Verstack_train_plots()
    fig = plt.figure(figsize=(11,3),dpi=300)
    P.plot_optimization_history(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=131)
    P.plot_param_imp(Storage.read_pd(key=f"{root_tag}/tuner_param_imp"),fig=fig,placement=132)
    P.plot_intermediate(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=133)
    plt.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save']+"BDT2_optimization",bbox_inches = 'tight',dpi=300)
    plt.show()

Verstack_Optimization_plots(root_tag='BDT2')

Storage.read_pd(key=f"{'BDT2'}/best_params")

def Predict(Storage, X_target=None, root_tag: str = 'BDT2'):
    X = X_target.copy()

    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()
    X = X[feat_idx]

    X_scaled = Utils.pd_scale(Storage, X, root_tag=root_tag)
    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}"] = BDT_scores

    return X_target

X_test_BDT2 = Predict(Storage, splits_BDT2['X_test'], root_tag='BDT2')

def BDT2_response_in_USR__NN2TRB_testset():
    P = plotting.plot_response(input_data=X_test_BDT2, threshold=0.7, root_tag='BDT2', test_data=True)

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0.975, 1))
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224)
    main_title = "GBDT2 Response in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}2^{\mathrm{data}}\}$ test set, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT2_test", bbox_inches='tight', dpi=300)

    plt.show()

BDT2_response_in_USR__NN2TRB_testset()

#  Training GBDT3 ( B vs Bbar / Mass Hypothesis )-----------------------------------------------------------------------

def Data_loader():
    print('Loading MC signal..')
    SIG_MC = MC_USR_lowq2
    SIG_MC.drop(SIG_MC[~((SIG_MC.info_is_true_nonres_Bd == True) | (SIG_MC.info_is_true_nonres_BdBar == True))].index,inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.info_is_true_nonres_Bd == True)].index[4*config['ML_config']['N_Signal']['Bd']:],inplace=True)
    SIG_MC.drop(SIG_MC[(SIG_MC.info_is_true_nonres_BdBar == True)].index[4*config['ML_config']['N_Signal']['BdBar']:],inplace=True)

    label = (SIG_MC.info_sample == 0)*0
    for i in config['labels_{} B_VS_BBar'.format(channel)]:
        label += (SIG_MC.info_sample == str(i))*config['labels_{} B_VS_BBar'.format(channel)][str(i)]
    SIG_MC['label'] = label

    return SIG_MC

B_vs_Bbar = Data_loader()

B_vs_Bbar = pd.concat([B_vs_Bbar])
splits_BDT3 = train_test_split_stratified(B_vs_Bbar)
Utils.fit_scaler_to_train(Storage,splits_BDT3['X_train'][features],root_tag='BDT3')
X_train3_scaled = Utils.pd_scale(Storage,splits_BDT3['X_train'][features], root_tag= 'BDT3', target_tag = 'X_train')
X_test3_scaled = Utils.pd_scale(Storage,splits_BDT3['X_test'][features], root_tag= 'BDT3', target_tag = 'X_test')

def Run_tuner(Storage=Storage, root_tag='BDT3'):
    assert str(type(Storage).__module__) == "Storage.Global", "Input Storage is not a correct global storage initialized from the HDF5_IO-function"

    np.random.seed(42)
    # print(f"Training LGBM with {Storage.read_pd(key=f"{root_tag}/X_train").shape[1]} features")
    tuner = verstack.LGBMTuner(metric='log_loss', trials=100, visualization=False, seed=42, verbosity=2, n_jobs=30)

    tuner.fit(X=X_train3_scaled, y=splits_BDT3['y_train'].astype(int).squeeze())

    Storage.write_pd(key=f"{root_tag}/tuner", df=tuner.study.trials_dataframe(attrs=('number', 'value', 'params', 'state', 'intermediate_values'), multi_index=True), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/tuner_param_imp", df=pd.DataFrame([dict(optuna.importance.get_param_importances(tuner.study).items())]).T.rename(columns={0: 'value'}), overwrite=True)
    Storage.save_lgbm(key=f"{root_tag}/model", tuner=tuner, feature_names=list(X_train2_scaled.columns), overwrite=True)
    Storage.write_pd(key=f"{root_tag}/best_params", df=pd.DataFrame([tuner.best_params]).T, overwrite=True)

    print('Done with training model')

    return tuner

BDT3_tuner = Run_tuner()

def Verstack_Optimization_plots(root_tag:str=None):
    P = plotting.Verstack_train_plots()
    fig = plt.figure(figsize=(11,3),dpi=300)
    P.plot_optimization_history(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=131)
    P.plot_param_imp(Storage.read_pd(key=f"{root_tag}/tuner_param_imp"),fig=fig,placement=132)
    P.plot_intermediate(Storage.read_pd(key=f"{root_tag}/tuner"),fig=fig,placement=133)
    plt.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save']+"BDT3_optimization",bbox_inches = 'tight',dpi=300)
    plt.show()

Verstack_Optimization_plots(root_tag='BDT3')

Storage.read_pd(key=f"{'BDT3'}/best_params")

def Predict(Storage, X_target=None, root_tag: str = 'BDT3'):
    X = X_target.copy()

    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()
    X = X[feat_idx]

    X_scaled = Utils.pd_scale(Storage, X, root_tag=root_tag)
    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}"] = BDT_scores

    return X_target

X_test_BDT3 = Predict(Storage, splits_BDT3['X_test'], root_tag='BDT3')

def BDT3_response_in_USR__NN2TRB_testset():
    P = plotting.plot_response(input_data=X_test_BDT3, threshold=0.5, root_tag='BDT3', test_data=True, MC_TEST=True)

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title, legend1='$\overline{B}_d^0$', legend2='$B_d^0$')
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0, 1), legend1='$\overline{B}_d^0$', legend2='$B_d^0$')
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224, legend1='$\overline{B}_d^0$', legend2='$B_d^0$')
    main_title = "GBDT3 Response in $\{\mathrm{SR}^{\mathrm{MC}}\}$ test set, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT3_test", bbox_inches='tight', dpi=300)

    plt.show()

BDT3_response_in_USR__NN2TRB_testset()

# Backup the storage hdf5 file is recommended --------------------------------------------------------------------------

import shutil

Storage_Location = config['PATHS_{}'.format(channel)]['Storage']
Storage_Copy_Location = config['PATHS_{}'.format(channel)]['Storage_Copy']
shutil.copy(Storage_Location, Storage_Copy_Location)

# Prediction on All data -----------------------------------------------------------------------------------------------

import tables
import h5py

def get_B_mass_true(df):
    print('Adding True Mass Column..')
    df["B_mass_true"] = ((df["info_is_true_nonres_Bd"]) | (df["info_is_true_res_Bd"])) * df["B_mass"] + ((df["info_is_true_nonres_BdBar"]) | (df["info_is_true_res_BdBar"])) * df["Bbar_mass"]
    return df

def Predict(Storage, X_target=None, root_tag: str = ''):
    X = X_target.copy()

    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()
    X = X[feat_idx]

    X_scaled = Utils.pd_scale(Storage, X, root_tag=root_tag)
    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}"] = BDT_scores

    return X_target

def predict_triple(Storage, X_target=None):
    print('Adding Predictions..')
    X = X_target.copy()

    # ------- BDT Sideband ----------
    root_tag = 'BDT1'
    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()
    X = X[feat_idx]

    X_scaled = Utils.pd_scale(Storage, X, root_tag=root_tag)
    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}"] = BDT_scores

    # ------- BDT Signal Region ----------
    root_tag = 'BDT2'
    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()
    X = X[feat_idx]

    X_scaled = Utils.pd_scale(Storage, X, root_tag=root_tag)
    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}"] = BDT_scores

    # ------- BDT B vs Bbar ----------
    root_tag = 'BDT3'
    model = Storage.load_lgbm(key=f"{root_tag}/model")
    feat_idx = model.feature_name()
    X = X[feat_idx]

    X_scaled = Utils.pd_scale(Storage, X, root_tag=root_tag)
    BDT_scores = model.predict(X_scaled.reindex(columns=feat_idx))

    X_target[f"P_{root_tag}"] = BDT_scores

    # !!!! MAX instead of min, but name is still min..
    X_target['P_BDT_max'] = np.min([X_target.P_BDT1, X_target.P_BDT2], axis=0)

    return X_target

def load_multiple_feather_files(file_list: list, selection: str, N_events: int, columns_to_read: list):
    print('Starts loading files..')
    files = []
    List_uniques_list = []
    for file_path in file_list:
        files.append(pd.read_feather(path=file_path, columns=columns_to_read, use_threads=True, storage_options=None))
    print('Done loading..')

    if selection == "None":
        for i in range(len(files)):
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]
    else:
        for i in range(len(files)):
            files[i] = files[i].query(selection)
            List_uniques = list(files[i]['info_event_number'].unique()[:N_events])
            List_uniques_list.append(List_uniques)
            files[i] = files[i][(files[i]['info_event_number'].isin(List_uniques))]

    print(f"Number of uniques: {[len(i) for i in List_uniques_list]}, total = {sum([len(i) for i in List_uniques_list])}")
    print(f"Length of files: {[len(i) for i in files]}, total = {sum([len(i) for i in files])}")

    df_files = pd.concat(files)
    df_files_w_true_mass = get_B_mass_true(df_files)
    df_files_w_true_mass_w_predictions = predict_triple(Storage, df_files_w_true_mass)
    print('Done..')
    print()

    return df_files_w_true_mass_w_predictions

if channel == "electron":
    files_dictionary = {
        'MC_signal': ['/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300590_part_01.ftr', # Using latest refit Feather Produced by KFung (6/2023)
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300590_part_02.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300590_part_03.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300590_part_04.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300591_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300591_part_02.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300592_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300592_part_02.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300593_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/electron/refit/feather/ntuple-300593_part_02.ftr'
                      ],
        'Data': sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if 'ntuple-data18_13TeV_periodK_part_1' in x]) # Using 10 feathers (PeriodK) for testing
    }

elif channel == "muon":
    files_dictionary = {
        'MC_signal': ['/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300700_part_01.ftr', # Using latest refit Feather Produced by KFung (6/2023)
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300700_part_02.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300701_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300701_part_02.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300702_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300703_part_01.ftr',
                      '/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon/refit/feather/ntuple-300703_part_02.ftr',
                      ],
        'Data': sorted([config['PATHS_{}'.format(channel)]['feather'] + x for x in os.listdir(config['PATHS_{}'.format(channel)]['feather']) if 'ntuple-data18_13TeV_periodK_part_1' in x]), # Using 10 feathers (PeriodK) for testing
    }

MC_USR_highq2 = load_multiple_feather_files(
    file_list = files_dictionary['MC_signal'],
    selection = Utils.selection_cuts()['usr_highq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)

MC_USR_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['MC_signal'],
    selection = Utils.selection_cuts()['usr_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)

NN1trb_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'], # ['/eos/home-c/cklo/LightGBM/data/Storages/NN1trb_lowq2_muon_PCA_ExtraFeatures.ftr']
    selection = Utils.selection_cuts()['nn1trb_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)

NN2trb_lowq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'], # ['/eos/home-c/cklo/LightGBM/data/Storages/NN2trb_lowq2_muon_PCA_ExtraFeatures.ftr']
    selection = Utils.selection_cuts()['nn2trb_lowq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)

Data_USR_highq2 = load_multiple_feather_files(
    file_list = files_dictionary['Data'], # ['/eos/home-c/cklo/LightGBM/data/Storages/Data_USR_highq2_muon_PCA.ftr']
    selection = Utils.selection_cuts()['usr_highq2'],
    N_events = -1,
    columns_to_read = Features_to_be_read)
#Data_USR_highq2.reset_index().to_feather("/eos/home-c/cklo/LightGBM/data/Storages/Data_USR_highq2_muon_PCA_ExtraFeatures.ftr")

def selections_predicter(input_data, THRESHOLD1: float = 0.5, THRESHOLD2: float = 0.5):
    PData = input_data.copy()
    # Global selection:
    if all(PData.info_sample != 'data'):
        PData = PData[(PData.P_BDT1 > THRESHOLD1) & (PData.P_BDT2 > THRESHOLD2)]
    else:
        PData = PData[(PData.P_BDT1 > THRESHOLD1) & (PData.P_BDT2 > THRESHOLD2)]

    # Local selection:
    PData = PData.sort_values('P_BDT_max').groupby(['info_event_number']).tail(1).sort_index()

    # Mass Hypothesis:
    y_predict = np.array(['BdBar' if pred > 0.5 else 'Bd' for pred in PData.P_BDT3])
    PData['mass_hypotesis'] = y_predict

    """
    PData['mass_hypotesis'] = 'NaN'
    max_select = np.argmax([PData.P_BDT_SB, PData.P_BDT_SR],axis=0)
    max_select_BDT1 = np.argmax([PData.P_BDT1_Bd, PData.P_BDT1_BdBar],axis=0)
    max_select_BDT2 = np.argmax([PData.P_BDT2_Bd, PData.P_BDT2_BdBar],axis=0)
    PData['mass_hypotesis'][(max_select==0)&(max_select_BDT1==0)] = 'Bd'
    PData['mass_hypotesis'][(max_select==0)&(max_select_BDT1==1)] = 'BdBar'
    PData['mass_hypotesis'][(max_select==1)&(max_select_BDT2==0)] = 'Bd'
    PData['mass_hypotesis'][(max_select==1)&(max_select_BDT2==1)] = 'BdBar'
    """

    def Apply_Mass_to_plot(df):
        df['B_mass_to_plot'] = np.zeros(len(df.B_mass))
        df['B_mass_to_plot'][(df.mass_hypotesis == 'BdBar')] = df.Bbar_mass
        df['B_mass_to_plot'][(df.mass_hypotesis == 'Bd')] = df.B_mass
        return df

    PData = Apply_Mass_to_plot(PData)

    return PData


def select_for_fitting(settings_input: tuple = ('Res', 'Bd'), Threshold1: list = None, Threshold2: list = None) -> None:
    assert settings_input[0] == ('Res' or 'NonRes'), "First entry in settings tuple needs top be either 'Res' or 'NonRes'."
    assert settings_input[1] == ('Bd' or 'BdBar'), "Second entry in settings tuple needs top be either 'Bd' or 'BdBar'."

    if settings_input == ('Res', 'Bd'):
        info_sample = mc_ResBd_str
    if settings_input == ('NonRes', 'Bd'):
        info_sample = mc_NonresBd_str
    if settings_input == ('Res', 'BdBar'):
        info_sample = mc_ResBdbar_str
    if settings_input == ('RonRes', 'BdBar'):
        info_sample = mc_NonresBdbar_str

    For_plotting = pd.DataFrame()
    for i, j in zip(Threshold1, Threshold2):
        MC_mass = selections_predicter(MC_USR_highq2, i, j)
        selected_MC = MC_mass[MC_mass.info_sample.isin([info_sample]) & (MC_mass.info_truth_matching == 1)].B_mass_true

        Data_mass = selections_predicter(Data_USR_highq2, i, j)
        selected_Data = Data_mass[(Data_mass.info_sample.isin(['data'])) & (Data_mass.mass_hypotesis == settings_input[1])].B_mass_to_plot
        savestring = settings_input[0] + settings_input[1]

        multiple_cuts = pd.DataFrame([selected_MC.values, selected_Data.values]).T.rename(columns={0: f"{savestring}MC_{i},{j}", 1: f"{savestring}Data_{i},{j}"})
        For_plotting = pd.concat([For_plotting, multiple_cuts], axis=1)

    For_plotting.reset_index(drop=True).to_feather(config['PATHS_{}'.format(channel)]['data'] + f'{savestring}_Highq2_for_fit.ftr')


THRESHOLD1_list = [0.65,0.675,0.7,0.725,0.75,0.775,0.8,0.825,0.85,0.875,0.9,0.925,0.95]
THRESHOLD2_list = [0.65,0.675,0.7,0.725,0.75,0.775,0.8,0.825,0.85,0.875,0.9,0.925,0.95]
select_for_fitting(('Res','Bd'),THRESHOLD1_list,THRESHOLD2_list)

def BDT1_response_in_USR__NN1TRB():
    P = plotting.plot_response(input_data=pd.concat([MC_USR_lowq2, NN1trb_lowq2], axis=0), threshold=0.7, root_tag='BDT1', MC_TEST=False)

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0.8, 1))
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224)
    main_title = "GBDT1 Response in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}1^{\mathrm{data}}\}$, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global/Local Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT1_USR_NN1TRB_lowq2", bbox_inches='tight', dpi=300)

    plt.show()

BDT1_response_in_USR__NN1TRB()

def BDT2_response_in_USR__NN2TRB():
    P = plotting.plot_response(input_data=pd.concat([MC_USR_lowq2, NN2trb_lowq2], axis=0), threshold=0.7, root_tag='BDT2')

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title)
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0.8, 1))
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224)
    main_title = "GBDT2 Response in $\{\mathrm{SR}^{\mathrm{MC}} \cup \mathrm{SB}2^{\mathrm{data}}\}$, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global/Local Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT2_USR_NN2TRB_lowq2", bbox_inches='tight', dpi=300)

    plt.show()

BDT2_response_in_USR__NN2TRB()

def BDT3_response_in_USR__NN2TRB():
    P = plotting.plot_response(input_data=pd.concat([MC_USR_lowq2], axis=0), threshold=0.5, root_tag='BDT3', test_data=False, MC_TEST=False)

    fig = plt.figure(figsize=(10, 6), dpi=300)
    title = ''
    P.plot_descision_scores(fig=fig, placemenent=221, title=title, legend1='$\overline{B}_d^0$', legend2='$B_d^0$')
    title = ''
    P.Plot_efficiency_rejection(fig=fig, placemenent=222, title=title, y_range=(0, 1), legend1='$\overline{B}_d^0$', legend2='$B_d^0$')
    P.plot_roc_curve(fig=fig, placemenent=223)
    P.plot_confusionmatrix_3x3(fig=fig, placemenent=224, legend1='$\overline{B}_d^0$', legend2='$B_d^0$')
    main_title = "GBDT3 Response in $\{\mathrm{SR}^{\mathrm{MC}}\}$ test set, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global Selection Rules Applied"
    fig.suptitle(main_title)
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT3", bbox_inches='tight', dpi=300)

    plt.show()

BDT3_response_in_USR__NN2TRB()

def plot_res_vs_nonres_wrapper():
    P = plotting.plot_res_vs_nonres(input_data=pd.concat([MC_USR_lowq2, MC_USR_highq2], axis=0), threshold=0.65)

    fig = plt.figure(figsize=(8, 3.5), dpi=300)
    # main_title = "GBDT's Response in $\{$SR $\cup$ SB2$\}$, $q_{low}^2$, Signal: $B_d^0$ & $\overline{B}_d^0$ | Global/Local Selection Rules Applied"

    main_title = "GBDT's efficiency in $q^2_{{low}}$ and $q^2_{{high}}$ | Global/Local Selection Rules Applied"
    fig.suptitle(main_title)
    title = 'GBDT1 Efficiency in $\{\mathrm{SR}^{\mathrm{MC}}\}$'
    P.Plot_2efficiency(fig=fig, placemenent=121, title=title, root_tag='BDT1')
    title = 'GBDT2 Efficiency in $\{\mathrm{SR}^{\mathrm{MC}}\}$'
    P.Plot_2efficiency(fig=fig, placemenent=122, title=title, root_tag='BDT2')
    fig.tight_layout()
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "BDT eff", bbox_inches='tight', dpi=300)

    plt.show()

plot_res_vs_nonres_wrapper()

def plot_mass_shape_in_USR_wrapper():
    P = plotting.plot_mass_shape_in_USR()

    fig = plt.figure(figsize=(12, 5), dpi=300)
    title = 'Shape of $m(B_{{closer}}^0)$ at different cuts on \n' + " GBDT1 scores for " + r"$\{\mathrm{SR}^{\mathrm{MC}}(B_d^0 \rightarrow K^{*0} l l)\}$, $q_{low}^2$ " + "\n Global, Local and Mass Hyp. Selection Rules Applied"
    P.plot_mass(fig=fig, placemenent=121, input_data=MC_USR_lowq2[(MC_USR_lowq2.info_sample == mc_NonresBd_str)], target='B_mass_Kstar_mass_closer', title=title, root_tag='BDT1')

    title = 'Shape of $m(B_{{closer}}^0)$ at different cuts on \n' + r" GBDT2 scores for $\{\mathrm{SR}^{\mathrm{MC}}(B_d^0 \rightarrow K^{*0} l l)\}$, $q_{low}^2$" + "\n Global, Local and Mass Hyp. Selection Rules Applied"
    P.plot_mass(fig=fig, placemenent=122, input_data=MC_USR_lowq2[(MC_USR_lowq2.info_sample == mc_NonresBd_str)], target='B_mass_Kstar_mass_closer', title=title, root_tag='BDT2')
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "Mass_shape_signal", bbox_inches='tight', dpi=300)

    plt.show()

plot_mass_shape_in_USR_wrapper()

def plot_mass_shape_in_USR_wrapper():
    P = plotting.plot_mass_shape_in_USR()

    fig = plt.figure(figsize=(12, 6), dpi=300)

    title = 'Shape of $m(B_{{closer}}^0)$ at different cuts on \n' + r" GBDT1 scores for $\{\mathrm{SB}2^{\mathrm{data}}\}$, $q_{low}^2$" + "\n Global, Local and Mass Hyp. Selection Rules Applied"
    P.plot_mass(fig=fig, placemenent=121, input_data=NN2trb_lowq2, target='B_mass_Kstar_mass_closer', title=title, root_tag='BDT1')

    title = 'Shape of $m(B_{{closer}}^0)$ at different cuts on \n' + r" GBDT2 scores for $\{\mathrm{SB}2^{\mathrm{data}}\}$, $q_{low}^2$" + "\n Global, Local and Mass Hyp. Selection Rules Applied"

    P.plot_mass(fig=fig, placemenent=122, input_data=NN2trb_lowq2, target='B_mass_Kstar_mass_closer', title=title, root_tag='BDT2')
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "Mass_shape_bkg", bbox_inches='tight', dpi=300)

    plt.show()

plot_mass_shape_in_USR_wrapper()

def plot_mass_shape_in_USR_wrapper():
    P = plotting.plot_mass_shape_in_USR()

    fig = plt.figure(figsize=(12, 5), dpi=300)
    title = 'Shape of $m(ll)$ at different cuts on \n' + r" GBDT1 scores for $\{\mathrm{SR}^{\mathrm{MC}}(B_d^0 \rightarrow K^{*0} l l)\}$, $q_{low}^2$" + "\n Global, Local and Mass Hyp. Selection Rules Applied"

    P.plot_mass(fig=fig, placemenent=121, input_data=MC_USR_lowq2[(MC_USR_lowq2.info_sample == mc_NonresBd_str)], target='di{}_mass'.format(lepton_big), title=title, root_tag='BDT1', xrange_bins=(1000, 2500, 100), x_label_str="$m(ll)$ $[MeV]$")

    title = 'Shape of $m(ll)$ at different cuts on \n' + r" GBDT2 scores for $\{\mathrm{SR}^{\mathrm{MC}}(B_d^0 \rightarrow K^{*0} l l)\}$, $q_{low}^2$" + "\n Global, Local and Mass Hyp. Selection Rules Applied"
    P.plot_mass(fig=fig, placemenent=122, input_data=MC_USR_lowq2[(MC_USR_lowq2.info_sample == mc_NonresBd_str)], target='di{}_mass'.format(lepton_big), title=title, root_tag='BDT2', xrange_bins=(1000, 2500, 100), x_label_str="$m(ll)$ $[MeV]$")
    plt.savefig(config['PATHS_{}'.format(channel)]['figure_save'] + "Mass_shape_mll", bbox_inches='tight', dpi=300)

    plt.show()

plot_mass_shape_in_USR_wrapper()